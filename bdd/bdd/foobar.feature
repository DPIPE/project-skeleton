Feature: Foo Bar form

    As a user I should be able to input a number in the form and see
    the result of the Foo Bar calculation.

    Scenario: Form test
        Given a browser
        When I visit "http://frontend:3000"
        Then I should see "Welcome to the FooBar service"
        When I fill in "number" with "1"
        And I press "Submit"
        Then I should see "Answer: 1"

        When I fill in "number" with "3"
        And I press "Submit"
        Then I should see "Answer: foo"

        When I fill in "number" with "5"
        And I press "Submit"
        Then I should see "Answer: bar"

        When I fill in "number" with "15"
        And I press "Submit"
        Then I should see "Answer: foobar"
