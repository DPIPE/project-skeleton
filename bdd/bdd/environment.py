from behaving.web import environment as benv


def before_all(context):
    context.remote_webdriver_url = (
        context.remote_webdriver_url
    ) = "http://selenoid:4444/wd/hub"
    context.base_url = "http://frontend:8000"
    context.default_browser = "chrome"
    context.screenshots_dir = "/app/screenshots"


def after_all(context):
    benv.after_all(context)


def before_feature(context, feature):
    benv.before_feature(context, feature)


def after_feature(context, feature):
    benv.after_feature(context, feature)


def before_scenario(context, scenario):
    benv.before_scenario(context, scenario)


def after_scenario(context, scenario):
    benv.after_scenario(context, scenario)
