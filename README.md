# Bioinf project skeleton/tutorial

This is a tutorial on setting up a bioinformatics project using python/react. It is intended to be used as a skeleton for new projects as well as a tutorial for new users.

## Tutorial structure

- [A basic python package](#basic-python-package)
- [Docker structure](#docker-structure)
- [Dev containers for vscode](#dev-containers-for-vscode)
- [Extending the python package with an API server](#extending-the-python-package-with-an-api-server)
- [Adding some tests, api endpoints](#adding-some-tests-api-endpoints)
- [A basic Next.js (react) app](#a-basic-nextjs-react-app)
- [End-to-end tests with Behavior-driven development](#end-to-end-tests-with-behavior-driven-development)
- [Command line interface](#command-line-interface)

## Basic python package

We will be creating a basic python package that will serve as our backend. We use [poetry](https://python-poetry.org) for dependency management and packaging. Poetry is a python package manager that is similar to pipenv. It is a bit more lightweight and has a few more features.

Start by creating a new directory for your project. We will call it `project-skeleton` for this tutorial. It will contain all the different parts of our project, such as the backend or frontend.

```bash
mkdir project-skeleton
cd project-skeleton
```

It's time to create our python package! It is good practice to create a virtual environment for your project. This will keep your dependencies isolated from the rest of your system. Later, when we will be using devcontainers, that environment will not be necessary, but it is useful to have it for local development.

```bash
python3 -m venv venv
```

Let's activate the virtual environment and install poetry.

```bash
source venv/bin/activate
pip install poetry
```

Now we can initialize our python package using poetry. We will call the package `backend`.

```bash
poetry new backend
```

This will create the `backend` folder for the python package, containing a `pyproject.toml` file with all the information about our package

```
[tool.poetry]
name = "backend"
version = "0.1.0"
description = ""
authors = ["Yiorgis Gozadinos <ggozadinos@gmail.com>"]
readme = "README.md"

[tool.poetry.dependencies]
python = "^3.10"


[build-system]
requires = ["poetry-core"]
build-backend = "poetry.core.masonry.api"
```

as well as a `README.md` file and a `backend` folder containing the actual python package.

While we are at it, let's add a few dependencies to our package. We will be using [black](https://black.readthedocs.io/en/stable/) for code formatting, [flake8](https://flake8.pycqa.org/) for linting and [pytest](https://docs.pytest.org/) for testing. Since these are development dependencies, we will be using the `--group dev` flag to add them to the project. That way they won't be packaged with the actual package on a release.

```bash
cd backend && poetry add --group dev black flake8 pytest
```

We can now install the dependencies using `poetry install`. This will install all the dependencies in the virtual environment.

```bash
poetry install
```

Let's test it:

```bash
python
Python 3.10.6 (main, Aug 30 2022, 05:12:36) [Clang 13.1.6 (clang-1316.0.21.2.5)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import backend
>>> print(backend)
<module 'backend' from '/Users/ggozad/dev/ous/project-skeleton/backend/backend/__init__.py'>
>>> quit()
```

And we can also run our (non-existent) tests:

```bash
pytest
======================= test session starts =======================
platform darwin -- Python 3.10.6, pytest-7.1.3, pluggy-1.0.0
rootdir: /Users/ggozad/dev/ous/project-skeleton/backend
collected 0 items

====================== no tests ran in 0.00s ======================
```

## Docker structure

In general a project might consist of several independent parts, such as a backend, a library, a database, some frontend apps and so on. We will be using docker compose to run all these parts together. The docker compose file is located in the root of the project. It will contain a `service` for each of the containers we want to run.

For example, the ELLA app contains services for backend, frontend, an nginx proxy and selenium (a service for running chrome, firefox and the selenium hub itself). The docker compose file looks as such in that case:

```yaml
version: "3.7"

services:
  chrome: ...
  firefox: ...
  selenium-hub: ...
  frontend: ...
  backend: ...
  nginx: ...
```

We will be using a similar structure for our project. We will have one service for the backend, and one for the frontend.
Let's start by containerizing our backend.

We add a `Dockerfile` that essentially copies the contents of the `backend` folder to the container and installs the dependencies.

```dockerfile
FROM python:3.10-slim-buster

ENV DEBIAN_FRONTEND=noninteractive
ENV LC_ALL=C.UTF-8 LANG=C.UTF-8

# Do not write .pyc files
ENV PYTHONDONTWRITEBYTECODE 1
# Do not ever buffer console output
ENV PYTHONUNBUFFERED 1

# We need gettext to get the envsubst command
# and git is nice to have in the dev container
RUN \
    apt-get update && \
    apt-get install --no-install-recommends -y gettext poppler-utils git && \
    rm -r /var/cache/apt /var/lib/apt

RUN pip install poetry

COPY poetry.lock pyproject.toml /app/
WORKDIR /app
RUN poetry config virtualenvs.create false
RUN poetry install
COPY backend /app/

RUN useradd -ms /bin/bash backend
RUN chown -R backend /app

USER backend

EXPOSE 8000

ENTRYPOINT ["tail", "-f", "/dev/null"]

```

It's time finally to add the service to the docker compose file. We will call it `backend` and we will use the `Dockerfile` we just created.

We create `docker-compose.yml` in the root of the project.

```yaml
version: "3.7"

services:
  backend:
    container_name: backend
    image: backend:dev
    build: ./backend
    volumes:
      - ./backend:/app # Mount the backend folder to the container to allow live reloading
```

We can now build and run the container

```bash
docker-compose build
docker-compose up
```

and test that the container is running by opening a shell in it.

```bash
docker exec -it backend bash
```

## Dev containers for vscode

It's time to add some [devcontainer](https://code.visualstudio.com/docs/remote/containers) configuration to our project. This will allow us to run the project in a container in vscode in order to have a consistent environment for all developers and avoid installing dependencies on the host machine.

Dev containers live in a `.devcontainer` folder in the root of the project. This folder contains a `devcontainer.json` that will be used to build the container.

We won't go in depth in the configuration of the dev container. It is set up to allow live reloading of the code, linting and formatting using `black` and `flake8` and running the tests using `pytest`.

To verify your installation open the `backend` folder with vscode. You should see a popup asking you to reopen the folder in a container. Click on `Reopen in container` and wait for the container to build. You should see a green box in the bottom left corner of the status bar indicating that the container is running.

You can now open a new terminal directly in the container by clicking on the `+` button in the terminal tab.

## Extending the python package with an API server

We will be using [FastAPI](https://fastapi.tiangolo.com/) to create our API server. FastAPI is a modern, fast web framework for building APIs with Python.

Open a terminal in the container and install fastapi and uvicorn.

```bash
poetry add fastapi
poetry add uvicorn
```

Create an entry point for the webserver to run inside the backend package. We will create an `app.py` file in the `backend` folder.

```python:
from fastapi import FastAPI

app = FastAPI()
```

Modify the `Dockerfile` to run the webserver by replacing the `ENTRYPOINT` with the following:

```dockerfile
CMD ["uvicorn", "backend.app:app", "--host", "0.0.0.0", "--port", "8000", "--reload", "--reload-dir", "/app/backend"]
```

Modify the `docker-compose.yml` to expose the port 8000.

```yaml
services:
  backend:
    ...
    ports:
      - "8000:8000"
```

If you now rebuild the container and run it you should be able to access the webserver at `http://localhost:8000/docs`.

## Adding some tests, api endpoints

Time for some (elementary) logic!

We'll be implementing the standard foobar problem. We will have a function that takes a number and returns a string. If the number is divisible by 3 it returns "foo", if it's divisible by 5 it returns "bar" and if it's divisible by both it returns "foobar". Otherwise it returns the number as a string.

We will create a `foobar.py` file in the `backend` folder.

```python
def foobar(number: int) -> str:
    if number % 3 == 0 and number % 5 == 0:
        return "foobar"
    if number % 3 == 0:
        return "foo"
    if number % 5 == 0:
        return "bar"
    return str(number)
```

We will also create a `test_foobar.py` file in the `backend/tests` folder.

```python
from backend.foobar import foobar

def test_foobar():
    assert foobar(1) == "1"
    assert foobar(3) == "foo"
    assert foobar(5) == "bar"
    assert foobar(15) == "foobar"
```

We can already run the tests by opening a terminal in the container and running `pytest`. We can also run the tests from vscode by clicking on the `Run all tests` button in the test explorer tab.

Let's now expose the foobar function to the API. We will create an `api` module in the `backend` folder. It's `__init__.py` will import all different api routers, in our case the `foobar` router and expose them as `api_router`.

```python
from fastapi import APIRouter
from backend.api import foobar

api_router = APIRouter()
api_router.include_router(foobar.router)
```

We then hook `api_router` to the FastAPI app in `app.py`.

```python
from backend.api import api_router
app.include_router(api_router, prefix="/api/v1")
```

We will then create a `foobar.py` file in the `api` folder where we will define the `foobar` POST route and its' request/response schemas.

```python
from backend.foobar import foobar
from fastapi import APIRouter
from pydantic import BaseModel

router = APIRouter()


class FoobarRequest(BaseModel):
    number: int


class FoobarResponse(BaseModel):
    result: str


@router.post("/foobar/", response_model=FoobarResponse)
def get_consumption(request: FoobarRequest):
    return FoobarResponse(result=foobar(request.number))
```

You should now be able to test the foobar endpoint at `http://localhost:8000/docs`.

While we are at it, we might as well add a test for the API endpoint. We will create a `test_foobar.py` file in the `backend/tests/api` folder.

```python
from fastapi.testclient import TestClient
from backend.app import app

client = TestClient(app)


def test_foobar():
    response = client.post("/api/v1/foobar/", json={"number": 1})
    assert response.status_code == 200
    assert response.json() == {"result": "1"}
```

Voila! We now have a working API server with tests.

## A basic Next.js (react) app

Time for some frontend!
Let's start by making a `frontend` folder in the root of the project. We will use [Next.js](https://nextjs.org/) to create a react app. Inside the `frontend` folder run the following command to create a new Next.js app.

```bash
npx create-next-app frontend
```

We will also create a `Dockerfile` for the frontend app,

```dockerfile
FROM node:16-alpine
RUN apk add --no-cache libc6-compat git

RUN mkdir /app
WORKDIR /app
RUN chown node:node -R /app

USER node
COPY frontend/package.json /app
COPY frontend/yarn.lock /app
RUN yarn

ADD frontend /app
WORKDIR /app/frontend

EXPOSE 3000
ENV NODE_ENV development
ENV PORT 3000

CMD ["yarn", "dev"]
```

as well as add a new service in our docker compose:

```yaml
services:
  backend: ...
  frontend:
    container_name: frontend
    image: frontend:dev
    build:
      context: ./frontend
      dockerfile: ./Dockerfile
    depends_on:
      - backend
    ports:
      - "3000:3000"
    volumes:
      - ./frontend:/app
      - /app/node_modules
```

Note that we mount the `frontend` folder as a volume so that we can edit the files in vscode and see the changes in the browser. We also mount the `node_modules` folder so that we cache the dependencies.

Finally we create a `.devcontainer` config to allow us to use vscode's dev containers.

If you now open the project in vscode you should be able to use the dev container. It will automatically build and start the backend container and the frontend app. You can then open the frontend app in the browser at `http://localhost:3000`.

Finally to complete the setup, let's add [Prettier](https://prettier.io/) to the frontend app. Open a terminal in your frontend dev container and run

```bash
yarn add --dev prettier eslint-config-prettier
```

adding also the following config to your package.json

```json
  "prettier": {
    "printWidth": 90,
    "trailingComma": "all",
    "tabWidth": 2,
    "semi": false,
    "singleQuote": true
  },
```

And edit your `.eslintrc.js` file to include the prettier config.

```js
{
  "extends": ["next/core-web-vitals", "prettier"]
}
```

You are now ready to start coding!

> We want to be accessing the backend from the frontend app both when we work locally and inside the containers. To do so we will use the `frontend`, `backend` as hostnames. When inside docker containers these are automatically resolved to the container's IP address. When working locally we will add these hostnames to our `/etc/hosts` file.

```bash
127.0.0.1 backend frontend
```

### Making a form

Let's make a basic form to call the foobar endpoint. We will edit the `pages/index.js` file in the `frontend` folder.

```jsx
import styles from "../styles/Home.module.css";
import { useState } from "react";

export default function Home() {
  const [number, setNumber] = useState(0);
  const [answer, setAnswer] = useState("");
  const onSubmit = (e) => {
    e.preventDefault();
    fetch("http://backend:8000/api/v1/foobar", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ number }),
    })
      .then((res) => res.json())
      .then((data) => setAnswer(data.result));
  };

  return (
    <div className={styles.container}>
      <main className={styles.main}>
        <h1 className={styles.title}>Welcome to the FooBar service</h1>
        <form onSubmit={onSubmit}>
          <label className={styles.description}>Your number</label>
          <input
            className={styles.input}
            type="text"
            name="name"
            onChange={(e) => setNumber(e.target.value)}
          />
        </form>
        <code className={styles.code}>Answer: {answer}</code>
      </main>
    </div>
  );
}
```

This should be obvious if you are familiar with React. If not, don't worry. We are just making a form that calls the foobar endpoint and displays the result. We use the `useState` hook to keep track of the form input and the result. We then use the `fetch` API to call the endpoint and display the result.

Before you try this out, you need to add the `http://localhost:3000` and `http://frontend:3000` to the CORS allowed origins in the backend app. You can do this by editing the `backend/app.py` file.

```python
app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:3000", "http://frontend:3000"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
```

You can see the form at `http://localhost:3000`. Give it a spin!

## End-to-end tests with Behavior-driven development

Now that we have a backend and a frontend, we want to create end-to-end tests for the whole system. We will write BDD tests that are written from the perspective of the user of our app. Essentially our tests are the "User Stories" of our app. They are written in a human-readable format (english in a specialized format called Gherkin) and should be easy to understand not only for engineers but also for non-technical people.

We will use [Selenium](https://www.selenium.dev/) to drive a browser and [Behaving](https://github.com/ggozad/behaving/) to write the tests.

### Plugging in Selenium

Hooking up the bdd package is slighly more complex than what we had to do for our backend. This is because BDD tests require browsers to run and we will be running inside containers. Therefore we need some additional containers that will be the hosts running the browsers. Typically [Selenium Grid](https://www.selenium.dev/documentation/en/grid/) is used, but here we will opt for the lighter [Selenoid](https://aerokube.com/selenoid/latest/).

Here's the `docker-compose.yml` file we will use for the bdd package.

```yaml
version: "3.7"
networks:
  bddnet:
    name: bddnet

services:

  backend:
    networks:
      - bddnet
    ...

  frontend:
    networks:
      - bddnet
    ...

  bdd:
    container_name: bdd
    image: behaving/behaving:latest
    depends_on:
      - backend
      - frontend
      - selenoid
    networks:
      - bddnet
    volumes:
      - ./bdd/bdd:/app/bdd
      - ./bdd/screenshots:/app/screenshots

  selenoid:
    container_name: selenoid
    image: aerokube/selenoid:latest-release
    networks:
      - bddnet
    volumes:
      - "./bdd/config:/etc/selenoid"
      - "/var/run/docker.sock:/var/run/docker.sock"
      - "./var/video:/opt/selenoid/video"
      - "./var/log:/opt/selenoid/logs"
    environment:
      - OVERRIDE_VIDEO_OUTPUT_DIR=./var/video
    command:
      [
        "-conf",
        "/etc/selenoid/browsers.json",
        "-video-output-dir",
        "/opt/selenoid/video",
        "-log-output-dir",
        "/opt/selenoid/logs",
        "-container-network",
        "bddnet",
      ]
    ports:
      - "4444:4444"

```

Notice how we have added a `selenoid` service in addition the `bdd` service. This is the container that will run the browsers. In addition we have added a custom `bddnet` network that will be used by all the containers and updated all the services to use it.

You should also manually pull images for the browsers you want to use. For example, to pull the latest version of Chrome or Firefox you can run:

```bash
docker pull selenoid/chrome:latest
docker pull selenoid/firefox:latest
```

NOTE: If you are running on `arm64` architecture, you will need to use the `arm64` versions of the browsers. Uncomment the line

```yaml
- "./bdd/config/browsers_arm64.json:/etc/selenoid/browsers.json"
```

in your docker compose and run

````bash
docker pull sskorol/selenoid_chromium_vnc:100.0
```
to pull the arm64 version of Chromium.
### Writing a test

To start, we need an `environment.py` file containing the test runner configuration. For behaving you typically place the tests (`.feature` files) and features in the same folder. Additionally you can place a `steps` folder with python files containing custom steps definitions. We will place all of these in the `bdd` folder.

Our `environment.py` file will look like this:

```python
from behaving.web import environment as benv


def before_all(context):
    context.remote_webdriver_url = (
        context.remote_webdriver_url
    ) = "http://selenoid:4444/wd/hub"
    context.base_url = "http://frontend:8000"
    context.default_browser = "chrome"
    context.screenshots_dir = "/app/var/screenshots"


def after_all(context):
    benv.after_all(context)


def before_feature(context, feature):
    benv.before_feature(context, feature)


def after_feature(context, feature):
    benv.after_feature(context, feature)


def before_scenario(context, scenario):
    benv.before_scenario(context, scenario)


def after_scenario(context, scenario):
    benv.after_scenario(context, scenario)

````

Notice how we have hooks that run before various events, such as before each feature or after all tests are done. These can be used to initialize things or clean up. Since we are only interested in doing testing through browsers we will be just calling the web environment hooks, and only really provide the `before_all` hook to provide basic information such as the remote webdriver url and the base url of our app. Similary, in the `steps` folder we create a `__init__.py` file that will import all the steps from the `behaving` package.

Now we can write our first test. This is shown in the `foobar.feature` file.

```gherkin
Feature: Foo Bar form

    As a user I should be able to input a number in the form and see
    the result of the Foo Bar calculation.

    Scenario: Form test
        Given a browser
        When I visit "http://frontend:3000"
        Then I should see "Welcome to the FooBar service"
        When I fill in "number" with "1"
        And I press "Submit"
        Then I should see "Answer: 1"

        When I fill in "number" with "3"
        And I press "Submit"
        Then I should see "Answer: foo"

        When I fill in "number" with "5"
        And I press "Submit"
        Then I should see "Answer: bar"

        When I fill in "number" with "15"
        And I press "Submit"
        Then I should see "Answer: foobar"
```

A `feature` file typically tests a single feature (or user story if you like). It is composed of a `Feature` header, a description of the feature, and a list of `Scenario` headers. Each scenario is a list of steps that are executed in order. `behaving` comes with lots of predefined steps useful for web apps but you can define more of your own.

Open a terminal inside the container and run the tests with:

```bash
behave bdd
```

You should see

```bash
Feature: Foo Bar form # bdd/foobar.feature:1
  As a user I should be able to input a number in the form and see
  the result of the Foo Bar calculation.
  Scenario: Form test                                 # bdd/foobar.feature:6
    Given a browser                                   # ../usr/local/lib/python3.10/site-packages/behaving/web/steps/browser.py:15 1.050s
    When I visit "http://frontend:3000"               # ../usr/local/lib/python3.10/site-packages/behaving/personas/persona.py:44 0.447s
    Then I should see "Welcome to the FooBar service" # ../usr/local/lib/python3.10/site-packages/behaving/personas/persona.py:44 0.079s
    When I fill in "number" with "1"                  # ../usr/local/lib/python3.10/site-packages/behaving/personas/persona.py:44 0.080s
    And I press "Submit"                              # ../usr/local/lib/python3.10/site-packages/behaving/personas/persona.py:44 0.042s
    Then I should see "Answer: 1"                     # ../usr/local/lib/python3.10/site-packages/behaving/personas/persona.py:44 0.018s
    When I fill in "number" with "3"                  # ../usr/local/lib/python3.10/site-packages/behaving/personas/persona.py:44 0.058s
    And I press "Submit"                              # ../usr/local/lib/python3.10/site-packages/behaving/personas/persona.py:44 0.029s
    Then I should see "Answer: foo"                   # ../usr/local/lib/python3.10/site-packages/behaving/personas/persona.py:44 0.017s
    When I fill in "number" with "5"                  # ../usr/local/lib/python3.10/site-packages/behaving/personas/persona.py:44 0.061s
    And I press "Submit"                              # ../usr/local/lib/python3.10/site-packages/behaving/personas/persona.py:44 0.039s
    Then I should see "Answer: bar"                   # ../usr/local/lib/python3.10/site-packages/behaving/personas/persona.py:44 0.017s
    When I fill in "number" with "15"                 # ../usr/local/lib/python3.10/site-packages/behaving/personas/persona.py:44 0.058s
    And I press "Submit"                              # ../usr/local/lib/python3.10/site-packages/behaving/personas/persona.py:44 0.028s
    Then I should see "Answer: foobar"                # ../usr/local/lib/python3.10/site-packages/behaving/personas/persona.py:44 0.018s

1 feature passed, 0 failed, 0 skipped
1 scenario passed, 0 failed, 0 skipped
15 steps passed, 0 failed, 0 skipped, 0 undefined
Took 0m2.042s
```

The tests are passing, and it's pretty fast!
Let's create a failure to see what happens. Change the `Then I should see "Answer: 1"` step to `Then I should see "Answer: 2"` and run the tests again. You should see something like this:

```bash
Feature: Foo Bar form # bdd/foobar.feature:1
  As a user I should be able to input a number in the form and see
  the result of the Foo Bar calculation.
  Scenario: Form test                                 # bdd/foobar.feature:6
    Given a browser                                   # ../usr/local/lib/python3.10/site-packages/behaving/web/steps/browser.py:15 1.041s
    When I visit "http://frontend:3000"               # ../usr/local/lib/python3.10/site-packages/behaving/personas/persona.py:44 0.481s
    Then I should see "Welcome to the FooBar service" # ../usr/local/lib/python3.10/site-packages/behaving/personas/persona.py:44 0.031s
    When I fill in "number" with "1"                  # ../usr/local/lib/python3.10/site-packages/behaving/personas/persona.py:44 0.081s
    And I press "Submit"                              # ../usr/local/lib/python3.10/site-packages/behaving/personas/persona.py:44 0.043s
    Then I should see "Answer: 2"                     # ../usr/local/lib/python3.10/site-packages/behaving/personas/persona.py:44 2.003s
      Assertion Failed: Text "Answer: 2" not found

    When I fill in "number" with "3"                  # None
    And I press "Submit"                              # None
    Then I should see "Answer: foo"                   # None
    When I fill in "number" with "5"                  # None
    And I press "Submit"                              # None
    Then I should see "Answer: bar"                   # None
    When I fill in "number" with "15"                 # None
    And I press "Submit"                              # None
    Then I should see "Answer: foobar"                # None


Failing scenarios:
  bdd/foobar.feature:6  Form test

0 features passed, 1 failed, 0 skipped
0 scenarios passed, 1 failed, 0 skipped
5 steps passed, 1 failed, 9 skipped, 0 undefined
```

To help you figure out what went wrong, behaving will take a screenshot of how the browser looked when a test fails. You can find the screenshot in the `bdd/screenshots` folder. If you open it you will see that the answer is 1, not 2 as we would expect.

## Command line interface

Finally, let's consider the case where we want to expose the application through a command line interface. We will use the [typer](https://typer.tiangolo.com/) library to create a CLI.

```bash
poetry add typer
```

(you can use `poetry add typer[all]` to install typer's dependencies if you want pretty colorful formatting!)

Create a new file `main.py` in the backend with the following code:

```python
from typing import Optional

import typer

from backend.foobar import foobar

app = typer.Typer()


@app.command()
def main(
    number: int = typer.Argument(..., help="The number to foobar"),
    capitalize: Optional[bool] = typer.Option(False, help="Capitalize the foobar"),
):
    """
    Give me a number and I'll give you foobars
    """
    if capitalize:
        print(foobar(number).capitalize())
    else:
        print(foobar(number))

```

Here we specify a command `main` that takes a number as an argument and an option to capitalize the result. We then call the `foobar` function and print the result. We also included a docstring that will be displayed when the user runs `python main.py --help` as well as help text for the arguments and options.

We would like when our package is installed to be able to run the command `foobar` from the command line. To do this, we need to add a `console_scripts` entry point to the `pyproject.toml` file.

```toml
[tool.poetry.scripts]
foobar = "backend.main:app"
```

If we now install our package and open a shell, we can access the command.

```bash
backend@1f29ffb00e90:/app$ poetry install
Installing dependencies from lock file

No dependencies to install or update

Installing the current project: backend (0.1.0)
backend@1f29ffb00e90:/app$ poetry shell
Spawning shell within /home/backend/.cache/pypoetry/virtualenvs/backend-9TtSrW0h-py3.10
backend@1f29ffb00e90:/app$ . /home/backend/.cache/pypoetry/virtualenvs/backend-9TtSrW0h-py3.10/bin/activate
(backend-py3.10) backend@1f29ffb00e90:/app$
(backend-py3.10) backend@1f29ffb00e90:/app$ foobar
Usage: foobar [OPTIONS] NUMBER
Try 'foobar --help' for help.
╭─ Error─────────────────────────────────────────╮
│ Missing argument 'NUMBER'.                     │
╰────────────────────────────────────────────────╯
(backend-py3.10) backend@1f29ffb00e90:/app$ foobar 3
foo
(backend-py3.10) backend@1f29ffb00e90:/app$
```

`typer` will automatically create commands for things like shell completion and can do a number of things. Try running `foobar --help` and look at its excellent documentation.

A simple test of the command line interface is included in `tests/test_main.py`.

Finnaly if you want to be able to call the command from the package itself, you need to provide it with a `__main__` module. Create a new file `backend/__main__.py` with the following code:

```python
from backend.main import app

app()
```

which merely calls the app. You can now do

```bash
backend@1f29ffb00e90:/app$ python -m backend 15
foobar
```
